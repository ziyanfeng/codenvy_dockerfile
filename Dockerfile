# Custom Codenvy image with Python 3

FROM codenvy/ubuntu_python:latest
MAINTAINER Alex Blanc "alex@blancsys.com"


# Base image use as default an user named "user"
# which have /home/user as home directory. Also,
# current directory is /projects. We change to
# root to install packages

ENV PACKAGES_REFRESHED_AT 2017_01_01
USER root
RUN apt-get update
RUN apt-get -y install libssl-dev  # For scrapy
RUN apt-get -y install git

# Install Python requirements
RUN pip install beautifulsoup4
RUN pip install numpy
RUN pip install scipy
RUN pip install pandas
RUN pip install scikit-learn
RUN pip install nltk
RUN pip install scrapy
# RUN pip install pattern # Pattern doesn't work on Python 3.5
RUN pip install theano
RUN pip install textblob
RUN pip install textacy
USER user

# Use CMD command to check if a SSH key for the user 'user' already exists. 
# If the SSH key doesn't exists, it's created (but not overwritten)
# Finally, we run the tail command used by the stock Codenvy images
# CMD sh -c "/usr/bin/test ! -e /home/alex/.ssh/id_rsa && /usr/bin/ssh-keygen -q -t rsa -b # 4096 -N '' -f /home/user/.ssh/id_rsa"; /usr/bin/tail -f /dev/null

